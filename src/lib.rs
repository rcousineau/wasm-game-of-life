extern crate cfg_if;
extern crate js_sys;
extern crate wasm_bindgen;
extern crate web_sys;

mod utils;

use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}

impl Cell {
    fn toggle(&mut self) {
        *self = match *self {
            Cell::Dead => Cell::Alive,
            Cell::Alive => Cell::Dead
        }
    }
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
    Dead = 0,
    Alive = 1
}

#[wasm_bindgen]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Vec<Cell>,
    debug: bool
}

#[wasm_bindgen]
impl Universe {
    pub fn new(universe_type: &str, debug: bool) -> Universe {
        utils::set_panic_hook();

        let width = 64;
        let height = 64;

        let cells = (0..width * height)
            .map(|_i| {
                match universe_type {
                    "random" => {
                        // random!
                        // -------
                        if js_sys::Math::random() < 0.5 {
                            Cell::Alive
                        } else {
                            Cell::Dead
                        }
                    },
                    "blank" => Cell::Dead,
                    _ => {
                        panic!(format!("unexpected universe_type: {}", universe_type));
                    }
                }
            })
            .collect();

        Universe {
            width,
            height,
            cells,
            debug
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn set_width(&mut self, width: u32) {
        self.width = width;
        self.cells = (0..width * self.height).map(|_i| Cell::Dead).collect();
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn set_height(&mut self, height: u32) {
        self.height = height;
        self.cells = (0..self.width * height).map(|_i| Cell::Dead).collect();
    }

    pub fn cells(&self) -> *const Cell {
        self.cells.as_ptr()
    }

    pub fn tick(&mut self) {
        let mut next = self.cells.clone();

        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells[idx];
                let live_neighbors = self.live_neighbor_count(row, col);

                let next_cell = match (cell, live_neighbors) {
                    // Rule 1: any live cell with fewer than 2 live neighbors dies
                    (Cell::Alive, x) if x < 2 => Cell::Dead,
                    // Rule 2: any live cell with 2 or 3 live neighbors lives
                    (Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
                    // Rule 3: any live cell with more than 3 live neighbors dies
                    (Cell::Alive, x) if x > 3 => Cell::Dead,
                    // Rule 4: any dead cell with exactly 3 live neighbors becomes alive
                    (Cell::Dead, 3) => Cell::Alive,
                    (otherwise, _) => otherwise
                };

                if self.debug && cell != next_cell {
                    log!(
                        "cell[{}, {}] was initially {:?} and has {} live neighbors",
                        row,
                        col,
                        cell,
                        live_neighbors
                    );
                    log!("    it becomes {:?}", next_cell);
                }

                next[idx] = next_cell;
            }
        }
        self.cells = next;
    }

    pub fn toggle_cell(&mut self, row: u32, column: u32) {
        let idx = self.get_index(row, column);
        self.cells[idx].toggle();
    }

    fn update_cell(&mut self, row: u32, column: u32, state: Cell) {
        let idx = self.get_index(row, column);
        self.cells[idx] = state;
    }

    fn update_cells(&mut self, updates: Vec<(u32, u32, Cell)>) {
        updates.iter().for_each(|update| {
            let (row, column, state) = update;
            self.update_cell(*row, *column, *state);
        });
    }

    pub fn create_glider(&mut self, row: u32, column: u32) {
        /*
        □ ■ □
        □ □ ■
        ■ ■ ■
        */
        self.update_cells(vec![
            (row - 1, column - 1, Cell::Dead),
            (row - 1, column,     Cell::Alive),
            (row - 1, column + 1, Cell::Dead),
            (row,     column - 1, Cell::Dead),
            (row,     column,     Cell::Dead),
            (row,     column + 1, Cell::Alive),
            (row + 1, column - 1, Cell::Alive),
            (row + 1, column,     Cell::Alive),
            (row + 1, column + 1, Cell::Alive)
        ]);
    }

    pub fn create_lightweight_spaceship(&mut self, row: u32, column: u32) {
        /*
        ■ □ □ ■ □
        □ □ □ □ ■
        ■ □ □ □ ■
        □ ■ ■ ■ ■
        □ □ □ □ □
        */
        self.update_cells(vec![
            (row - 2, column - 2, Cell::Alive),
            (row - 2, column - 1, Cell::Dead),
            (row - 2, column,     Cell::Dead),
            (row - 2, column + 1, Cell::Alive),
            (row - 2, column + 2, Cell::Dead),

            (row - 1, column - 2, Cell::Dead),
            (row - 1, column - 1, Cell::Dead),
            (row - 1, column,     Cell::Dead),
            (row - 1, column + 1, Cell::Dead),
            (row - 1, column + 2, Cell::Alive),

            (row,     column - 2, Cell::Alive),
            (row,     column - 1, Cell::Dead),
            (row,     column,     Cell::Dead),
            (row,     column + 1, Cell::Dead),
            (row,     column + 2, Cell::Alive),

            (row + 1, column - 2, Cell::Dead),
            (row + 1, column - 1, Cell::Alive),
            (row + 1, column,     Cell::Alive),
            (row + 1, column + 1, Cell::Alive),
            (row + 1, column + 2, Cell::Alive),

            (row + 2, column - 2, Cell::Dead),
            (row + 2, column - 1, Cell::Dead),
            (row + 2, column,     Cell::Dead),
            (row + 2, column + 1, Cell::Dead),
            (row + 2, column + 2, Cell::Dead),
        ]);
    }

    pub fn create_middleweight_spaceship(&mut self, row: u32, column: u32) {
        /*
        □ □ □ □ □ □ □
        □ □ □ □ ■ □ □
        □ □ ■ □ □ □ ■
        □ ■ □ □ □ □ □
        □ ■ □ □ □ □ ■
        □ ■ ■ ■ ■ ■ □
        □ □ □ □ □ □ □
        */
        self.update_cells(vec![
            (row - 3, column - 3, Cell::Dead),
            (row - 3, column - 2, Cell::Dead),
            (row - 3, column - 1, Cell::Dead),
            (row - 3, column,     Cell::Dead),
            (row - 3, column + 1, Cell::Dead),
            (row - 3, column + 2, Cell::Dead),
            (row - 3, column + 3, Cell::Dead),

            (row - 2, column - 3, Cell::Dead),
            (row - 2, column - 2, Cell::Dead),
            (row - 2, column - 1, Cell::Dead),
            (row - 2, column,     Cell::Dead),
            (row - 2, column + 1, Cell::Alive),
            (row - 2, column + 2, Cell::Dead),
            (row - 2, column + 3, Cell::Dead),

            (row - 1, column - 3, Cell::Dead),
            (row - 1, column - 2, Cell::Dead),
            (row - 1, column - 1, Cell::Alive),
            (row - 1, column,     Cell::Dead),
            (row - 1, column + 1, Cell::Dead),
            (row - 1, column + 2, Cell::Dead),
            (row - 1, column + 3, Cell::Alive),

            (row,     column - 3, Cell::Dead),
            (row,     column - 2, Cell::Alive),
            (row,     column - 1, Cell::Dead),
            (row,     column,     Cell::Dead),
            (row,     column + 1, Cell::Dead),
            (row,     column + 2, Cell::Dead),
            (row,     column + 3, Cell::Dead),

            (row + 1, column - 3, Cell::Dead),
            (row + 1, column - 2, Cell::Alive),
            (row + 1, column - 1, Cell::Dead),
            (row + 1, column,     Cell::Dead),
            (row + 1, column + 1, Cell::Dead),
            (row + 1, column + 2, Cell::Dead),
            (row + 1, column + 3, Cell::Alive),

            (row + 2, column - 3, Cell::Dead),
            (row + 2, column - 2, Cell::Alive),
            (row + 2, column - 1, Cell::Alive),
            (row + 2, column,     Cell::Alive),
            (row + 2, column + 1, Cell::Alive),
            (row + 2, column + 2, Cell::Alive),
            (row + 2, column + 3, Cell::Dead),

            (row + 3, column - 3, Cell::Dead),
            (row + 3, column - 2, Cell::Dead),
            (row + 3, column - 1, Cell::Dead),
            (row + 3, column,     Cell::Dead),
            (row + 3, column + 1, Cell::Dead),
            (row + 3, column + 2, Cell::Dead),
            (row + 3, column + 3, Cell::Dead),
        ]);
    }

    pub fn create_heavyweight_spaceship(&mut self, row: u32, column: u32) {
        /*
        □ □ □ □ □ □ □
        ■ ■ ■ ■ ■ ■ □
        ■ □ □ □ □ □ ■
        ■ □ □ □ □ □ □
        □ ■ □ □ □ □ ■
        □ □ □ ■ ■ □ □
        □ □ □ □ □ □ □
        */
        self.update_cells(vec![
            (row - 3, column - 3, Cell::Dead),
            (row - 3, column - 2, Cell::Dead),
            (row - 3, column - 1, Cell::Dead),
            (row - 3, column,     Cell::Dead),
            (row - 3, column + 1, Cell::Dead),
            (row - 3, column + 2, Cell::Dead),
            (row - 3, column + 3, Cell::Dead),

            (row - 2, column - 3, Cell::Alive),
            (row - 2, column - 2, Cell::Alive),
            (row - 2, column - 1, Cell::Alive),
            (row - 2, column,     Cell::Alive),
            (row - 2, column + 1, Cell::Alive),
            (row - 2, column + 2, Cell::Alive),
            (row - 2, column + 3, Cell::Dead),

            (row - 1, column - 3, Cell::Alive),
            (row - 1, column - 2, Cell::Dead),
            (row - 1, column - 1, Cell::Dead),
            (row - 1, column,     Cell::Dead),
            (row - 1, column + 1, Cell::Dead),
            (row - 1, column + 2, Cell::Dead),
            (row - 1, column + 3, Cell::Alive),

            (row,     column - 3, Cell::Alive),
            (row,     column - 2, Cell::Dead),
            (row,     column - 1, Cell::Dead),
            (row,     column,     Cell::Dead),
            (row,     column + 1, Cell::Dead),
            (row,     column + 2, Cell::Dead),
            (row,     column + 3, Cell::Dead),

            (row + 1, column - 3, Cell::Dead),
            (row + 1, column - 2, Cell::Alive),
            (row + 1, column - 1, Cell::Dead),
            (row + 1, column,     Cell::Dead),
            (row + 1, column + 1, Cell::Dead),
            (row + 1, column + 2, Cell::Dead),
            (row + 1, column + 3, Cell::Alive),

            (row + 2, column - 3, Cell::Dead),
            (row + 2, column - 2, Cell::Dead),
            (row + 2, column - 1, Cell::Dead),
            (row + 2, column,     Cell::Alive),
            (row + 2, column + 1, Cell::Alive),
            (row + 2, column + 2, Cell::Dead),
            (row + 2, column + 3, Cell::Dead),

            (row + 3, column - 3, Cell::Dead),
            (row + 3, column - 2, Cell::Dead),
            (row + 3, column - 1, Cell::Dead),
            (row + 3, column,     Cell::Dead),
            (row + 3, column + 1, Cell::Dead),
            (row + 3, column + 2, Cell::Dead),
            (row + 3, column + 3, Cell::Dead),
        ]);
    }

    fn get_index(&self, row: u32, column: u32) -> usize {
        ((row % self.height) * self.width + (column % self.width)) as usize
    }

    fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        let mut count = 0;
        for delta_row in [self.height - 1, 0, 1].iter().cloned() {
            for delta_col in [self.width - 1, 0, 1].iter().cloned() {
                if delta_row == 0 && delta_col == 0 {
                    continue;
                }

                let neighbor_row = (row + delta_row) % self.height;
                let neighbor_col = (column + delta_col) % self.width;
                let idx = self.get_index(neighbor_row, neighbor_col);
                count += self.cells[idx] as u8;
            }
        }
        count
    }
}

// used only for testing
impl Universe {
    pub fn get_cells(&self) -> &[Cell] {
        &self.cells
    }

    pub fn set_cells(&mut self, cells: &[(u32, u32)]) {
        for (row, col) in cells.iter().cloned() {
            let idx = self.get_index(row, col);
            self.cells[idx] = Cell::Alive;
        }
    }
}

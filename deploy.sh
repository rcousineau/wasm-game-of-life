#!/bin/bash

# Deploy built project to gitlab pages
# https://rcousineau.gitlab.io/conway

SCRIPT_DIR=$( cd "$(dirname "$0")" ; pwd -P )

function deploy_debug {
  # print pwd relative to project root
  CURRENT_DIR=$(python -c "import os.path; print(os.path.relpath('$PWD', '$SCRIPT_DIR'))")
  if [[ $CURRENT_DIR == "." ]]; then
    echo -n "[deploy.sh] "
  else
    echo -n "[deploy.sh ($CURRENT_DIR)] "
  fi
  echo $1
}

# build wasm

deploy_debug '`wasm-pack build`...'
wasm-pack build

# build www

pushd pkg
deploy_debug '`npm link`...'
npm link
popd

pushd www
deploy_debug '`npm install`...'
npm install
deploy_debug '`npm link wasm-game-of-life`...'
npm link wasm-game-of-life
deploy_debug '`npm run build`...'
npm run build
popd

# add ssh agent identity

ssh-add -l > /dev/null
if [[ $? -ne 0 ]]; then
  deploy_debug '`ssh-add`...'
  ssh-add
fi

# clone gitlab pages repo if a checkout doesn't exist already

if [[ ! -d ".gitlab-pages" ]]; then
  git clone git@gitlab.com:rcousineau/rcousineau.gitlab.io.git .gitlab-pages
fi

pushd .gitlab-pages

# pull to ensure clone is up-to-date

git pull

# copy files in place

deploy_debug "copying files in place..."
mkdir -p public/conway
cp -vR ../www/dist/* public/conway/

# commit

git add public/conway
git commit -m 'update rust wasm conway game of life'

# push

git push

popd

# link to gitlab CI dashboard

echo 'https://gitlab.com/rcousineau/rcousineau.gitlab.io/pipelines'

# link to published page

echo 'https://rcousineau.gitlab.io/conway'

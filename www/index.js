import { Cell, Universe } from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg";

const CELL_SIZE = 8;
const GRID_COLOR = "#CCC";
const DEAD_COLOR = "#FFF";
const ALIVE_COLOR = "#000";

let started;
let startHandler;
let playPauseHandler;
let canvasClickHandler;

const canvas = document.getElementById("game-of-life-canvas");
const startButton = document.getElementById("start-button");
const playPauseButton = document.getElementById("play-pause-button");
const patternSelect = document.getElementById("pattern-select");

startButton.disabled = true;
playPauseButton.disabled = true;

const loadUniverse = (universeType) => {
  started = null;

  const universe = Universe.new(universeType, false);
  const width = universe.width();
  const height = universe.height();

  canvas.width = (CELL_SIZE + 1) * width + 1;
  canvas.height = (CELL_SIZE + 1) * height + 1;

  const ctx = canvas.getContext("2d");

  const drawGrid = () => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    // vertical lines
    for (let i = 0; i <= width; i++) {
      ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
      ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
    }

    // horizontal lines
    for (let j = 0; j <= height; j++) {
      ctx.moveTo(0,                           j * (CELL_SIZE + 1) + 1);
      ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
    }

    ctx.stroke();
  };

  const getIndex = (row, column) => {
    return row * width + column;
  };

  const drawCells = () => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

    ctx.beginPath();

    for (let row = 0; row < height; row++) {
      for (let col = 0; col < width; col++) {
        const idx = getIndex(row, col);

        ctx.fillStyle = cells[idx] === Cell.Dead
          ? DEAD_COLOR
          : ALIVE_COLOR;

        ctx.fillRect(
          col * (CELL_SIZE + 1) + 1,
          row * (CELL_SIZE + 1) + 1,
          CELL_SIZE,
          CELL_SIZE
        );
      }
    }

    ctx.stroke();
  };

  let animationId = null;

  const isPaused = () => {
    return animationId === null;
  };

  const renderLoop = () => {
    // when the current `universeType` doesn't match the one we `started` with,
    // it's time to exit the render loop and stop animating
    if (started !== universeType) {
      return;
    }

    universe.tick();

    drawGrid();
    drawCells();
    animationId = requestAnimationFrame(renderLoop);
  };

  drawGrid();
  drawCells();

  if (startHandler) {
    startButton.removeEventListener("click", startHandler);
  }

  startHandler = () => {
    startButton.disabled = true;
    started = universeType;

    const play = () => {
      playPauseButton.textContent = "⏸";
      renderLoop();
    };

    const pause = () => {
      playPauseButton.textContent = "▶";
      cancelAnimationFrame(animationId);
      animationId = null;
    };

    if (playPauseHandler) {
      playPauseButton.removeEventListener("click", playPauseHandler);
    }

    playPauseHandler = () => {
      if (isPaused()) {
        play();
      } else {
        pause();
      }
    };

    playPauseButton.disabled = false;
    playPauseButton.addEventListener("click", playPauseHandler);

    play();
  };

  if (canvasClickHandler) {
    canvas.removeEventListener("click", canvasClickHandler);
  }

  canvasClickHandler = (event) => {
    const boundingRect = canvas.getBoundingClientRect();

    const scaleX = canvas.width / boundingRect.width;
    const scaleY = canvas.height / boundingRect.height;

    const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
    const canvasTop = (event.clientY - boundingRect.top) * scaleY;

    const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
    const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

    if (patternSelect.value === "cell") {
      universe.toggle_cell(row, col);
    } else if (patternSelect.value === "glider") {
      universe.create_glider(row, col);
    } else if (patternSelect.value === "lwss") {
      universe.create_lightweight_spaceship(row, col);
    } else if (patternSelect.value === "mwss") {
      universe.create_middleweight_spaceship(row, col);
    } else if (patternSelect.value === "hwss") {
      universe.create_heavyweight_spaceship(row, col);
    } else {
      console.error("unexpected pattern '" + patternSelect.value + "' selected");
    }

    drawGrid();
    drawCells();
  };

  canvas.addEventListener("click", canvasClickHandler);

  startButton.disabled = false;
  startButton.addEventListener("click", startHandler);

  playPauseButton.disabled = true;
  playPauseButton.textContent = "⏸";
};

const randomButton = document.getElementById("random-universe");
randomButton.addEventListener("click", () => {
  loadUniverse("random");
});

const clearButton = document.getElementById("clear-universe");
clearButton.addEventListener("click", () => {
  loadUniverse("blank");
});

loadUniverse("blank");
